const test = require('tape')
const { isValid } = require('@tangle/strategy')
const Strategy = require('../')

test('Strategy.isValid', t => {
  t.true(isValid(Strategy()), 'is a valid strategy according to @tangle/strategy')
  if (isValid.error) console.log(isValid.error)

  t.end()
})

const {
  identity,
  concat,
  mapToOutput,
  mapFromInput
} = Strategy()

const Ts = [
  { faerie: 1 },
  { faerie: 0 },
  { faerie: -1 },
  { faerie: 1, taniwha: 2 },
  { taniwha: 2, faerie: 1 }, // order-invariance
  { faeire: 0, taniwha: 2 },
  identity() // {}
]

test('mapToOutput', t => {
  t.deepEqual(
    Ts.map(mapToOutput),
    [
      ['faerie'], // { faeire: 1 },
      [], // { faeire: 0 },
      [], // { faeire: -1 },
      ['faerie', 'taniwha'], // { faerie: 1, taniwha: 2 },
      ['faerie', 'taniwha'], // { taniwha: 1, faerie: 2 }, order-invariance
      ['taniwha'], // { faeire: 0, taniwha: 2 },
      [] // identity() // {}
    ],
    'mapToOutput transformation (general + identity)'
  )
  t.end()
})

test('concat, identity, associativity', t => {
  t.deepEqual(concat(identity(), { a: 1 }), { a: 1 }, 'identiy (left)')
  t.deepEqual(concat({ a: 1 }, identity()), { a: 1 }, 'identity (right)')

  const A = { a: 1 }
  const B = { a: 1, b: 1 }
  const C = { b: 1, c: 1 }

  t.deepEqual(
    concat(concat(A, B), C),
    concat(A, concat(B, C)),
    'associativity'
  )

  // BONUS: inverses
  const T1 = { a: 1 }
  const T2 = { a: -1 }
  t.deepEqual(concat(T1, T2), identity(), 'inverse (part I)')
  t.deepEqual(concat(T2, T1), identity(), 'inverse (part II)')

  // BONUS: commutative
  const U1 = { a: 1, b: 3, c: -2 }
  const U2 = { a: 2, b: -1, c: 2 }
  t.deepEqual(concat(U1, U2), concat(U2, U1), 'commutative')
  t.end()
})

test('mapFromInput (valid)', t => {
  const initialStates = [
    {}, // 1: add to empty state
    { cherese: 1 }, // 2: add to a state with an existing item
    { cherese: 1 },
    {
      mixmix: -2,
      mixmixjellyfish: -1,
      'j a irving': 1
    },
    {
      cherese: -1
    },
    {},
    {},
    {},
    {},
    {}
  ]

  const updates = [
    {
      add: ['cherese']
    },
    {
      add: ['cheche']
    },
    {
      remove: ['cherese']
    },
    {
      add: ['mixmixjellyfish', 'jai'],
      remove: ['j a irving']
    },
    {
      add: ['cherese']
    },
    {
      add: []
    },
    undefined,
    null
  ]

  const expectedStates = [
    {
      cherese: 1
    },
    {
      cheche: 1
    },
    { cherese: -2 },
    {
      mixmixjellyfish: 2,
      jai: 1,
      'j a irving': -2
    },
    {
      cherese: 2
    },
    {},
    undefined,
    undefined
  ]

  initialStates.forEach((initialState, i) => {
    t.deepEqual(
      mapFromInput(
        updates[i],
        [initialState]
      ),
      expectedStates[i],
      JSON.stringify(updates[i])
    )
  })

  t.end()
})

test('mapFromInput (invalid)', t => {
  const updates = [
    'dog',
    1234,
    false,
    true,
    { add: 1 },
    { add: null },
    { add: 'dog' },
    { remove: 1 },
    {}
  ]

  updates.forEach((update) => {
    t.throws(
      () => mapFromInput(
        update,
        [identity()] // empty initial state
      ),
      `${JSON.stringify(update)} => throws`
    )
  })

  t.end()
})

test('mapFromInput (multiple tips)', t => {
  t.deepEqual(mapFromInput({ add: ['colin'] }, [
    { cherese: 1 },
    { mix: -2 }
  ]), {
    colin: 1
  }, 'Other fields are ignored when adding')

  t.deepEqual(mapFromInput({ remove: ['colin'] }, [
    { colin: 1, cherese: 1 },
    { mix: -2 }
  ]), {
    colin: -2
  }, 'Other fields are ignored when removing')

  t.deepEqual(mapFromInput({ add: ['colin'] }, [
    { colin: 1 },
    { colin: -2 }
  ]), {
    colin: 2
  }, 'Adding will aim for +1 if negative')

  t.deepEqual(mapFromInput({ remove: ['colin'] }, [
    { colin: 2 },
    { colin: -4 }
  ]), {}, 'Removing will not change if already negative')
  t.deepEqual(mapFromInput({ add: ['colin'] }, [
    { colin: 4 },
    { colin: 2 }
  ]), {}, 'Adding will not change if already positive')

  t.throws(
    () => mapFromInput(
      { remove: ['colin'] },
      identity()
    ), /currentTips.map is not a function/ // Consider throwing a more helpful error
  )

  t.end()
})
