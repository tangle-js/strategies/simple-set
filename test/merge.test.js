const test = require('tape')
const Strategy = require('../')
const tangle = require('@tangle/test')

const {
  identity,
  isConflict,
  isValidMerge,
  merge
} = Strategy()

test('merging', t => {
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'simple' })
  const mergeGraph = buildGraph(`
     A-->B->D
     A-->C->D
   `)
  const mergeNode = mergeGraph.getNode('D')
  t.equal(isConflict(mergeGraph, ['B', 'C']), false, 'no conflict')
  t.equal(isValidMerge(mergeGraph, mergeNode), true, 'merge is always fine')
  // merge
  t.deepEqual(merge(buildGraph(`
     A-->B
     A-->C
   `), mergeNode, 'simple'), identity(), ' identity merge')
  t.deepEqual(merge(buildGraph(`
     A[{a:1}]-->B
     A--------->C
   `), mergeNode, 'simple'), { a: 1 }, 'merge with root value')
  t.deepEqual(merge(buildGraph(`
     A[{a:1}]-->B[{a:-1}]->D
     A--------->C--------->D
   `), mergeNode, 'simple'), { }, 'merge one branch removes root')
  t.deepEqual(merge(buildGraph(`
     A[{a:1}]-->B[{a:-1}]->D
     A--------->C[{b:1}]-->D
   `), mergeNode, 'simple'), { b: 1 }, 'merge two branches')

  mergeNode.data.simple = { a: 1, b: -1 }
  t.deepEqual(merge(buildGraph(`
     A[{a:1}]-->B[{a:-1}]->D[{a:1, b:-1}]
     A--------->C[{b:1}]-->D
   `), mergeNode, 'simple'), { a: 1 }, 'merge all nodes')

  t.end()
})
