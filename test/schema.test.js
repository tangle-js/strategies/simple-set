const test = require('tape')
const Validator = require('is-my-json-valid')
const Strategy = require('../')

test('schema', t => {
  const { schema, identity } = Strategy()
  const isValid = Validator(schema)

  const Ts = [
    { faerie: 1 },
    { faerie: 0 },
    { faerie: -1 },
    { faerie: 1, taniwha: 2 },
    { taniwha: 2, faerie: 1 }, // order-invariance
    { faeire: 0, taniwha: 2 },
    identity() // {}
  ]

  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    'dog',
    undefined,
    null,
    true,
    { faeire: 2.5 },
    { faerie: 'duck' },
    { faerie: null },
    { faerie: true }
    /* these two aren't invalidated */
    // { cat: 2, faerie: undefined },
    // { faerie: undefined }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})

test('schema (custom)', t => {
  const { schema, identity } = Strategy('^\\w+(worth|wain|wright)$')
  const isValid = Validator(schema)

  const Ts = [
    { wadsworth: 1 },
    { poopswain: 0 },
    { herpwright: -1 },
    identity() // {}
  ]

  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    'dog',
    undefined,
    null,
    true,
    { wright: 1 }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})
