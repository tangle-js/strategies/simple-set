# @tangle/simple-set

see tests for examples.

## API

### `SimpleSet(idPattern) => simpleSet`

- `idPattern` *String* (optional)
    - a pattern which all id's going into the set must pass
    - default `'^.+$'`

### `simpleSet.schema`
### `simpleSet.isValid`
### `simpleSet.identity() => I`
### `simpleSet.concat() => I`
### `simpleSet.mapFromInput(input, currentTips) => T`
### `simpleSet.mapToOutput(T) => t`

### `simpleSet.isConflict() => False`
### `simpleSet.isValidMerge() => True`
### `simpleSet.merge(graph, mergeNode, field) => T`
where:
- `graph` is a `@tangle/graph` instance
- `mergeNode` is the proposed merge-node
- `field` *String* contains the the data fields `node.data[field]`

Used with modules such as:
- @tangle/strategy@2
- ssb-crut
