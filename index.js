const Validator = require('is-my-json-valid')

// This made for modelling Sets made up of Strings
//
// I've chosen to make it *commutative* (transformation order doesn't matter)
// Implications of this are:
// - no merge conflicts!
// - more concise transformations
// - reified output is always alphabetically sorted

const IDENTITY = {}

module.exports = function SimpleSet (idPattern) {
  // transformations are of form:
  // - { String: Integer }
  // - {} (identity)

  if (!idPattern) idPattern = '^.+$'
  const schema = {
    type: 'object',
    patternProperties: {
      [idPattern]: { type: 'integer' }
    },
    additionalProperties: false
  }
  const isValid = Validator(schema)

  function mapToOutput (T) {
    if (!isValid(T)) throw Error('reify expects valid transformation')

    return Object.entries(T)
      .map(([entry, weight]) => {
        return weight > 0 ? entry : null
      })
      .filter(Boolean)
      .sort()
  }

  function concat (a, b) {
    if (!isValid(a)) throw Error('concat expects valid transformation')
    if (!isValid(b)) throw Error('concat expects valid transformation')

    const newT = Object.assign({}, a)
    Object.entries(b).forEach(([entry, weight]) => {
      if (entry in newT) {
        const newWeight = newT[entry] + weight

        if (newWeight === 0) delete newT[entry]
        else newT[entry] = newWeight
      } else {
        newT[entry] = weight
      }
    })

    return newT
  }

  /*
    gets the new state from the current state + a change for a simple set
  */
  function mapFromInput (input, currentTips) {
    if (input === null || input === undefined) return undefined

    if (typeof input !== 'object') throw new Error('input must be an object')
    if (typeof input === 'object' && !Object.keys(input).length) throw new Error('input must be an object of shape { add: [String], remove: [String] }')

    const { add = [], remove = [] } = input

    if (add && !Array.isArray(add)) throw new Error('simpleSet.add must be an Array of Strings')
    if (remove && !Array.isArray(remove)) throw new Error('simpleSet.remove must be an Array of Strings')

    const change = {}

    add.forEach(key => {
      const current = currentTips.map(currentT => { return currentT[key] || 0 })
        .reduce((a, b) => a + b, 0)
      if (current <= 0) {
        change[key] = (-1 * current) + 1
      }
    })

    remove.forEach(key => {
      const current = currentTips.map(currentT => { return currentT[key] || 0 })
        .reduce((a, b) => a + b, 0)
      if (current >= 0) {
        change[key] = (-1 * current) - 1
      }
    })

    return change
  }

  function isConflict () {
    // Could consider returning true if the signs of the fields in each branch are opposites
    return false
  }

  function isValidMerge () {
    return true
  }

  // Concat the history of the mergenode in any order
  function merge (graph, mergeNode, field) {
    // Build a set of the keys in the history of mergeNode
    const keyHistory = new Set(mergeNode.previous)
    for (const prevKey of mergeNode.previous) {
      for (const historyKey of graph.getHistory(prevKey)) {
        keyHistory.add(historyKey)
      }
    }
    // Concat all of the fields in the history together
    const concatHistory = Array.from(keyHistory).map((key) => {
      if (field in graph.getNode(key).data) return graph.getNode(key).data[field]
      else return IDENTITY
    }).reduce((acc, next) => concat(acc, next), IDENTITY)
    // Concat the merge node field as well.
    const mergeNodeField = field in mergeNode.data ? mergeNode.data[field] : IDENTITY
    return concat(concatHistory, mergeNodeField)
  }

  return {
    schema,
    isValid,
    identity: () => IDENTITY,
    concat,
    mapFromInput,
    mapToOutput,
    isConflict,
    isValidMerge,
    merge
  }
}
